#pragma checksum "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4eb7350e5897c89e271efcc5b62f67cfb3cc3956"
// <auto-generated/>
#pragma warning disable 1591
namespace CUnity.Client.Shared.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#nullable restore
#line 1 "C:\Projects\CUnity\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Projects\CUnity\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared.Extensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Projects\CUnity\Client\_Imports.razor"
using MatBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Shared.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Axes;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Axes.Ticks;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Handlers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Time;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Util;

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Interop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#nullable disable
    public partial class AdminTopBarRightSection : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Authorization.AuthorizeView>(0);
            __builder.AddAttribute(1, "Authorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((AuthorizeContext) => (__builder2) => {
                __builder2.OpenComponent<MatBlazor.MatTooltip>(2);
                __builder2.AddAttribute(3, "Tooltip", "Log out");
                __builder2.AddAttribute(4, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<MatBlazor.ForwardRef>)((context) => (__builder3) => {
                    __builder3.OpenElement(5, "h1");
                    __builder3.AddContent(6, "Hello, ");
                    __builder3.AddContent(7, 
#nullable restore
#line 9 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
                        AuthorizeContext.User.Identity.Name

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddContent(8, "!");
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(9, "\r\n            ");
                    __builder3.OpenComponent<MatBlazor.MatIconButton>(10);
                    __builder3.AddAttribute(11, "Icon", "exit_to_app");
                    __builder3.AddAttribute(12, "RefBack", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MatBlazor.ForwardRef>(
#nullable restore
#line 12 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
                                                        context

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(13, "OnClick", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 12 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
                                                                          LogoutClick

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.AddAttribute(14, "NotAuthorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((AuthorizeContext) => (__builder2) => {
                __builder2.OpenComponent<MatBlazor.MatTooltip>(15);
                __builder2.AddAttribute(16, "Tooltip", "Login");
                __builder2.AddAttribute(17, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<MatBlazor.ForwardRef>)((context) => (__builder3) => {
                    __builder3.OpenComponent<MatBlazor.MatIconButton>(18);
                    __builder3.AddAttribute(19, "Icon", "account_box");
                    __builder3.AddAttribute(20, "RefBack", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MatBlazor.ForwardRef>(
#nullable restore
#line 17 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
                                                        context

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(21, "Link", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 17 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
                                                                        Constants.LoginPath

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 22 "C:\Projects\CUnity\Client\Shared\Components\AdminTopBarRightSection.razor"
         
    async Task LogoutClick()
    {
        //appState.ClearUserProfile();
        await (authStateProvider).Logout();
        navigationManager.NavigateTo(Constants.HomePath, forceLoad: true);

    } 

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private AppState appState { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private CustomStateProvider authStateProvider { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
    }
}
#pragma warning restore 1591
