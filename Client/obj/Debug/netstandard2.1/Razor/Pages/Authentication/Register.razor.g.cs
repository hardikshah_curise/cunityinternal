#pragma checksum "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4c61d1b9ed037ed510a691c9a7f3351da218b05a"
// <auto-generated/>
#pragma warning disable 1591
namespace CUnity.Client.Pages.Authentication
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Projects\CUnity\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Projects\CUnity\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared.Extensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Projects\CUnity\Client\_Imports.razor"
using MatBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Shared.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Axes;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Axes.Ticks;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Handlers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Time;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Util;

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Interop;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(LoginLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/register_front")]
    public partial class Register : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<MatBlazor.MatCard>(0);
            __builder.AddAttribute(1, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenElement(2, "div");
                __builder2.AddAttribute(3, "class", "hidden-mdc-down");
                __builder2.OpenComponent<MatBlazor.MatIconButton>(4);
                __builder2.AddAttribute(5, "Icon", "home");
                __builder2.AddAttribute(6, "Class", "float-left");
                __builder2.AddAttribute(7, "Link", "/");
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(8, "\r\n            ");
                __builder2.AddMarkupContent(9, "<div class=\"logo\"><img src=\"images/logo.svg\" style=\"width:100px;\"><br>CUnity\r\n                <br>\r\n                <h4>Registration</h4></div>\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(10);
                __builder2.AddAttribute(11, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 16 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                              registerViewModel

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(12, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 16 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                 OnSubmit

#line default
#line hidden
#nullable disable
                )));
                __builder2.AddAttribute(13, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder3) => {
                    __builder3.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(14);
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(15, "\r\n                ");
                    __builder3.OpenComponent<Microsoft.AspNetCore.Components.Forms.ValidationSummary>(16);
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(17, "\r\n                ");
                    __builder3.OpenElement(18, "fieldset");
                    __builder3.OpenElement(19, "div");
                    __builder3.AddAttribute(20, "class", "form-group");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateMatTextField_0(__builder3, 21, 22, "First Name", 23, "mail_outline", 24, 
#nullable restore
#line 21 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                      true

#line default
#line hidden
#nullable disable
                    , 25, 
#nullable restore
#line 21 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                       true

#line default
#line hidden
#nullable disable
                    , 26, 
#nullable restore
#line 21 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                                       true

#line default
#line hidden
#nullable disable
                    , 27, 
#nullable restore
#line 21 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                    registerViewModel.FirstName

#line default
#line hidden
#nullable disable
                    , 28, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.FirstName = __value, registerViewModel.FirstName)), 29, () => registerViewModel.FirstName);
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(30, "\r\n                    ");
                    __builder3.OpenElement(31, "div");
                    __builder3.AddAttribute(32, "class", "form-group");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateMatTextField_1(__builder3, 33, 34, "Last Name", 35, "mail_outline", 36, 
#nullable restore
#line 24 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                    true

#line default
#line hidden
#nullable disable
                    , 37, 
#nullable restore
#line 24 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                     true

#line default
#line hidden
#nullable disable
                    , 38, 
#nullable restore
#line 24 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                                     true

#line default
#line hidden
#nullable disable
                    , 39, 
#nullable restore
#line 24 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                    registerViewModel.LastName

#line default
#line hidden
#nullable disable
                    , 40, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.LastName = __value, registerViewModel.LastName)), 41, () => registerViewModel.LastName);
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(42, "\r\n                    ");
                    __builder3.OpenElement(43, "div");
                    __builder3.AddAttribute(44, "class", "form-group");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateMatTextField_2(__builder3, 45, 46, "UserName", 47, "person", 48, 
#nullable restore
#line 27 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                             true

#line default
#line hidden
#nullable disable
                    , 49, 
#nullable restore
#line 27 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                              true

#line default
#line hidden
#nullable disable
                    , 50, 
#nullable restore
#line 27 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                              true

#line default
#line hidden
#nullable disable
                    , 51, 
#nullable restore
#line 27 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                    registerViewModel.UserName

#line default
#line hidden
#nullable disable
                    , 52, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.UserName = __value, registerViewModel.UserName)), 53, () => registerViewModel.UserName);
                    __builder3.AddMarkupContent(54, "\r\n                        ");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateValidationMessage_3(__builder3, 55, 56, 
#nullable restore
#line 28 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                  () => registerViewModel.UserName

#line default
#line hidden
#nullable disable
                    );
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(57, "\r\n                    ");
                    __builder3.OpenElement(58, "div");
                    __builder3.AddAttribute(59, "class", "form-group");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateMatTextField_4(__builder3, 60, 61, "Email", 62, "mail_outline", 63, 
#nullable restore
#line 32 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                             true

#line default
#line hidden
#nullable disable
                    , 64, 
#nullable restore
#line 32 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                              true

#line default
#line hidden
#nullable disable
                    , 65, 
#nullable restore
#line 32 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                              true

#line default
#line hidden
#nullable disable
                    , 66, 
#nullable restore
#line 32 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                    registerViewModel.Email

#line default
#line hidden
#nullable disable
                    , 67, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.Email = __value, registerViewModel.Email)), 68, () => registerViewModel.Email);
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(69, "\r\n                    ");
                    __builder3.OpenElement(70, "div");
                    __builder3.AddAttribute(71, "class", "form-group");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateMatTextField_5(__builder3, 72, 73, "Password", 74, "lock_outline", 75, 
#nullable restore
#line 35 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                   true

#line default
#line hidden
#nullable disable
                    , 76, 
#nullable restore
#line 35 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                    true

#line default
#line hidden
#nullable disable
                    , 77, 
#nullable restore
#line 35 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                                    true

#line default
#line hidden
#nullable disable
                    , 78, "password", 79, 
#nullable restore
#line 35 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                    registerViewModel.Password

#line default
#line hidden
#nullable disable
                    , 80, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.Password = __value, registerViewModel.Password)), 81, () => registerViewModel.Password);
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(82, "\r\n                    ");
                    __builder3.OpenElement(83, "div");
                    __builder3.AddAttribute(84, "class", "form-group");
                    __Blazor.CUnity.Client.Pages.Authentication.Register.TypeInference.CreateMatTextField_6(__builder3, 85, 86, "Password Confirmation", 87, "lock_outline", 88, 
#nullable restore
#line 38 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                       true

#line default
#line hidden
#nullable disable
                    , 89, 
#nullable restore
#line 38 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                                        true

#line default
#line hidden
#nullable disable
                    , 90, 
#nullable restore
#line 38 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                                                                                                                                                        true

#line default
#line hidden
#nullable disable
                    , 91, "password", 92, 
#nullable restore
#line 38 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                    registerViewModel.PasswordConfirm

#line default
#line hidden
#nullable disable
                    , 93, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.PasswordConfirm = __value, registerViewModel.PasswordConfirm)), 94, () => registerViewModel.PasswordConfirm);
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(95, "\r\n                    ");
                    __builder3.OpenElement(96, "div");
                    __builder3.AddAttribute(97, "class", "form-group d-flex justify-content-end");
                    __builder3.OpenComponent<MatBlazor.MatButton>(98);
                    __builder3.AddAttribute(99, "Type", "submit");
                    __builder3.AddAttribute(100, "Raised", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 41 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                                                         true

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(101, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(102, "Register");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(103, "\r\n                    <hr>\r\n                    ");
                    __builder3.OpenComponent<Microsoft.AspNetCore.Components.Routing.NavLink>(104);
                    __builder3.AddAttribute(105, "href", "login");
                    __builder3.AddAttribute(106, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddMarkupContent(107, "<h6 class=\"font-weight-normal text-center\">Already have an account? Click here to login</h6>");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.CloseElement();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 74 "C:\Projects\CUnity\Client\Pages\Authentication\Register.razor"
                    RegisterRequest registerViewModel { get; set; } = new RegisterRequest();

            async Task OnSubmit()
            {
                try
                {
                    registerViewModel.RoleName = "Administrator";
                    await authStateProvider.Register(registerViewModel);
                    // var roleresult = _userManager.AddToRolesAsync(currentUser, "Administrator");
                    matToaster.Add("Registration Success", MatToastType.Success, "");
                    navigationManager.NavigateTo("");
                }
                catch (Exception ex)
                {
                    matToaster.Add(ex.Message, MatToastType.Danger, "Registration Faild");
                }
            } 

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAuthService api { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IMatToaster matToaster { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private CustomStateProvider authStateProvider { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
    }
}
namespace __Blazor.CUnity.Client.Pages.Authentication.Register
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateMatTextField_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_4<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_5<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, global::System.String __arg5, int __seq6, TValue __arg6, int __seq7, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg7, int __seq8, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg8)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Type", __arg5);
        __builder.AddAttribute(__seq6, "Value", __arg6);
        __builder.AddAttribute(__seq7, "ValueChanged", __arg7);
        __builder.AddAttribute(__seq8, "ValueExpression", __arg8);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_6<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, global::System.String __arg5, int __seq6, TValue __arg6, int __seq7, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg7, int __seq8, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg8)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Type", __arg5);
        __builder.AddAttribute(__seq6, "Value", __arg6);
        __builder.AddAttribute(__seq7, "ValueChanged", __arg7);
        __builder.AddAttribute(__seq8, "ValueExpression", __arg8);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
