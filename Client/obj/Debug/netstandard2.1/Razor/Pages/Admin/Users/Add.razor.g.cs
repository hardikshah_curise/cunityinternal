#pragma checksum "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d978bcc1081d30530141143562a622411d491211"
// <auto-generated/>
#pragma warning disable 1591
namespace CUnity.Client.Pages.Admin.Users
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Projects\CUnity\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Projects\CUnity\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared.Extensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Projects\CUnity\Client\_Imports.razor"
using MatBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Projects\CUnity\Client\_Imports.razor"
using CUnity.Client.Shared.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Projects\CUnity\Client\_Imports.razor"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Axes;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Axes.Ticks;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Handlers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Common.Time;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Util;

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "C:\Projects\CUnity\Client\_Imports.razor"
using ChartJs.Blazor.Interop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
           [Authorize(Roles = "Administrator,SuperAdmin")]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(AdminLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/admin/users/add")]
    public partial class Add : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(0);
            __builder.AddAttribute(1, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 18 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                  registerViewModel

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 18 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                     OnSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(4);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(5, "\r\n    ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.ValidationSummary>(6);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(7, "\r\n    ");
                __builder2.OpenElement(8, "fieldset");
#nullable restore
#line 23 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
         if (AdminLayout.IsSuperAdmin)
        {

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(9, "div");
                __builder2.AddAttribute(10, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatSelect_0(__builder2, 11, 12, 
#nullable restore
#line 28 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                     true

#line default
#line hidden
#nullable disable
                , 13, "Client", 14, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, 
#nullable restore
#line 28 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                        (string i) => OnChangeCategory(i)

#line default
#line hidden
#nullable disable
                ), 15, (__builder3) => {
                    __builder3.OpenComponent<MatBlazor.MatOptionString>(16);
                    __builder3.AddAttribute(17, "Value", "-1");
                    __builder3.AddAttribute(18, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(19, "Select Client");
                    }
                    ));
                    __builder3.CloseComponent();
#nullable restore
#line 30 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                     foreach (var cl in Clients)
                    {

#line default
#line hidden
#nullable disable
                    __builder3.OpenComponent<MatBlazor.MatOptionString>(20);
                    __builder3.AddAttribute(21, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 32 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                 cl.ToString()

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(22, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(23, 
#nullable restore
#line 32 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                 cl.ToString()

#line default
#line hidden
#nullable disable
                        );
                    }
                    ));
                    __builder3.CloseComponent();
#nullable restore
#line 33 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                    }

#line default
#line hidden
#nullable disable
                }
                );
                __builder2.AddMarkupContent(24, "\r\n                ");
                __builder2.OpenElement(25, "span");
                __builder2.AddContent(26, 
#nullable restore
#line 35 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                       selectedValue

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.CloseElement();
#nullable restore
#line 38 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
        }

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(27, "div");
                __builder2.AddAttribute(28, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatTextField_1(__builder2, 29, 30, "First Name", 31, "mail_outline", 32, 
#nullable restore
#line 40 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                          true

#line default
#line hidden
#nullable disable
                , 33, 
#nullable restore
#line 40 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                           true

#line default
#line hidden
#nullable disable
                , 34, 
#nullable restore
#line 40 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                           true

#line default
#line hidden
#nullable disable
                , 35, 
#nullable restore
#line 40 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                        registerViewModel.FirstName

#line default
#line hidden
#nullable disable
                , 36, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.FirstName = __value, registerViewModel.FirstName)), 37, () => registerViewModel.FirstName);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(38, "\r\n        ");
                __builder2.OpenElement(39, "div");
                __builder2.AddAttribute(40, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatTextField_2(__builder2, 41, 42, "Last Name", 43, "mail_outline", 44, 
#nullable restore
#line 43 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                        true

#line default
#line hidden
#nullable disable
                , 45, 
#nullable restore
#line 43 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                         true

#line default
#line hidden
#nullable disable
                , 46, 
#nullable restore
#line 43 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                         true

#line default
#line hidden
#nullable disable
                , 47, 
#nullable restore
#line 43 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                        registerViewModel.LastName

#line default
#line hidden
#nullable disable
                , 48, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.LastName = __value, registerViewModel.LastName)), 49, () => registerViewModel.LastName);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(50, "\r\n        ");
                __builder2.OpenElement(51, "div");
                __builder2.AddAttribute(52, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatTextField_3(__builder2, 53, 54, "UserName", 55, "person", 56, 
#nullable restore
#line 46 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                 true

#line default
#line hidden
#nullable disable
                , 57, 
#nullable restore
#line 46 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                  true

#line default
#line hidden
#nullable disable
                , 58, 
#nullable restore
#line 46 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                  true

#line default
#line hidden
#nullable disable
                , 59, 
#nullable restore
#line 46 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                        registerViewModel.UserName

#line default
#line hidden
#nullable disable
                , 60, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.UserName = __value, registerViewModel.UserName)), 61, () => registerViewModel.UserName);
                __builder2.AddMarkupContent(62, "\r\n            ");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateValidationMessage_4(__builder2, 63, 64, 
#nullable restore
#line 47 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                      () => registerViewModel.UserName

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(65, "\r\n        ");
                __builder2.OpenElement(66, "div");
                __builder2.AddAttribute(67, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatTextField_5(__builder2, 68, 69, "Email", 70, "mail_outline", 71, 
#nullable restore
#line 51 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                 true

#line default
#line hidden
#nullable disable
                , 72, 
#nullable restore
#line 51 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                  true

#line default
#line hidden
#nullable disable
                , 73, 
#nullable restore
#line 51 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                  true

#line default
#line hidden
#nullable disable
                , 74, 
#nullable restore
#line 51 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                        registerViewModel.Email

#line default
#line hidden
#nullable disable
                , 75, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.Email = __value, registerViewModel.Email)), 76, () => registerViewModel.Email);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(77, "\r\n        ");
                __builder2.OpenElement(78, "div");
                __builder2.AddAttribute(79, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatTextField_6(__builder2, 80, 81, "Password", 82, "lock_outline", 83, 
#nullable restore
#line 54 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                       true

#line default
#line hidden
#nullable disable
                , 84, 
#nullable restore
#line 54 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                        true

#line default
#line hidden
#nullable disable
                , 85, 
#nullable restore
#line 54 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                        true

#line default
#line hidden
#nullable disable
                , 86, "password", 87, 
#nullable restore
#line 54 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                        registerViewModel.Password

#line default
#line hidden
#nullable disable
                , 88, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.Password = __value, registerViewModel.Password)), 89, () => registerViewModel.Password);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(90, "\r\n        ");
                __builder2.OpenElement(91, "div");
                __builder2.AddAttribute(92, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatTextField_7(__builder2, 93, 94, "Password Confirmation", 95, "lock_outline", 96, 
#nullable restore
#line 57 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                           true

#line default
#line hidden
#nullable disable
                , 97, 
#nullable restore
#line 57 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                            true

#line default
#line hidden
#nullable disable
                , 98, 
#nullable restore
#line 57 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                                                                                                                            true

#line default
#line hidden
#nullable disable
                , 99, "password", 100, 
#nullable restore
#line 57 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                        registerViewModel.PasswordConfirm

#line default
#line hidden
#nullable disable
                , 101, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.PasswordConfirm = __value, registerViewModel.PasswordConfirm)), 102, () => registerViewModel.PasswordConfirm);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(103, "\r\n        ");
                __builder2.OpenElement(104, "div");
                __builder2.AddAttribute(105, "class", "form-group");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateMatSelectItem_8(__builder2, 106, 107, 
#nullable restore
#line 60 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                                                             Roles

#line default
#line hidden
#nullable disable
                , 108, "Select Role Name", 109, 
#nullable restore
#line 60 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                         registerViewModel.RoleName

#line default
#line hidden
#nullable disable
                , 110, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => registerViewModel.RoleName = __value, registerViewModel.RoleName)), 111, () => registerViewModel.RoleName);
                __builder2.AddMarkupContent(112, "\r\n            ");
                __Blazor.CUnity.Client.Pages.Admin.Users.Add.TypeInference.CreateValidationMessage_9(__builder2, 113, 114, 
#nullable restore
#line 61 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                      () => registerViewModel.RoleName

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(115, "\r\n        ");
                __builder2.OpenElement(116, "div");
                __builder2.AddAttribute(117, "class", "form-group d-flex justify-content-end");
                __builder2.OpenComponent<MatBlazor.MatButton>(118);
                __builder2.AddAttribute(119, "Type", "submit");
                __builder2.AddAttribute(120, "Raised", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 64 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
                                             true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(121, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddContent(122, "Register");
                }
                ));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(123, "\r\n        <hr>");
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 74 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
       
    RegisterRequest registerViewModel { get; set; } = new RegisterRequest();
    List<string> Roles { get; set; } = new List<string>() { "User" };
    public List<string> Clients { get; set; } = new List<string>();
    public Client[] lstClients { get; set; }
    public string selectedValue;
    public int ClientId { get; set; }
    protected override async Task OnInitializedAsync()
    {
        try
        {
            var lstClients = await client.GetFromJsonAsync<Client[]>("api/subscription/GetClient");
            Clients = lstClients.Select(p => p.Name).ToList();

        }
        catch (Exception ex)
        {
            matToaster.Add(ex.GetBaseException().Message, MatToastType.Danger, "Operation Failed");
        }
    }
    protected void OnChangeCategory(string value)
    {
        //do something
        selectedValue = "Selected Value: " + value;
    }
    async Task OnSubmit()
    {
        try
        {
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 103 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
             if (AdminLayout.IsSuperAdmin)
            {
                var res = lstClients.Where(p => p.Name == registerViewModel.ClientName).FirstOrDefault();
                ClientId = res.Id;    // If logged in user is Super Admin then get the SEelcted Client Id
            }
            else
            {
                ClientId = Convert.ToInt32(((CurrentUser)(await authStateProvider.GetCurrentUser())).ClientId); // if client admin is logged in then select its Clientid as Default Client id
            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 111 "C:\Projects\CUnity\Client\Pages\Admin\Users\Add.razor"
             
            var DomainEmail = lstClients.Where(p => p.Id == ClientId).FirstOrDefault().Email;
            registerViewModel.ClientEmail = DomainEmail;
            registerViewModel.ClientId = ClientId;

            //registerViewModel.RoleName = "Administrator";
            //registerViewModel.ClientId = 1;
            await authStateProvider.Register(registerViewModel);
            // var roleresult = _userManager.AddToRolesAsync(currentUser, "Administrator");
            matToaster.Add("Registration Success", MatToastType.Success, "");
            navigationManager.NavigateTo("/admin/users");
        }
        catch (Exception ex)
        {
            matToaster.Add(ex.Message, MatToastType.Danger, "Registration Faild");
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private CustomStateProvider authStateProvider { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IMatToaster matToaster { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient client { get; set; }
    }
}
namespace __Blazor.CUnity.Client.Pages.Admin.Users.Add
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateMatSelect_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Boolean __arg0, int __seq1, global::System.String __arg1, int __seq2, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg2, int __seq3, global::Microsoft.AspNetCore.Components.RenderFragment __arg3)
        {
        __builder.OpenComponent<global::MatBlazor.MatSelect<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Outlined", __arg0);
        __builder.AddAttribute(__seq1, "Label", __arg1);
        __builder.AddAttribute(__seq2, "ValueChanged", __arg2);
        __builder.AddAttribute(__seq3, "ChildContent", __arg3);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_4<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_5<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, TValue __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg6, int __seq7, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg7)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.AddAttribute(__seq7, "ValueExpression", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_6<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, global::System.String __arg5, int __seq6, TValue __arg6, int __seq7, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg7, int __seq8, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg8)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Type", __arg5);
        __builder.AddAttribute(__seq6, "Value", __arg6);
        __builder.AddAttribute(__seq7, "ValueChanged", __arg7);
        __builder.AddAttribute(__seq8, "ValueExpression", __arg8);
        __builder.CloseComponent();
        }
        public static void CreateMatTextField_7<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Boolean __arg4, int __seq5, global::System.String __arg5, int __seq6, TValue __arg6, int __seq7, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg7, int __seq8, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg8)
        {
        __builder.OpenComponent<global::MatBlazor.MatTextField<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Icon", __arg1);
        __builder.AddAttribute(__seq2, "IconTrailing", __arg2);
        __builder.AddAttribute(__seq3, "FullWidth", __arg3);
        __builder.AddAttribute(__seq4, "Required", __arg4);
        __builder.AddAttribute(__seq5, "Type", __arg5);
        __builder.AddAttribute(__seq6, "Value", __arg6);
        __builder.AddAttribute(__seq7, "ValueChanged", __arg7);
        __builder.AddAttribute(__seq8, "ValueExpression", __arg8);
        __builder.CloseComponent();
        }
        public static void CreateMatSelectItem_8<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Collections.Generic.IReadOnlyList<TValue> __arg0, int __seq1, global::System.String __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::MatBlazor.MatSelectItem<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Items", __arg0);
        __builder.AddAttribute(__seq1, "Label", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_9<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
